package baseClass;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;


public class SeleniumBase {

	public static RemoteWebDriver driver;

	@BeforeTest
	public void openUrl() {

		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}
	
	public WebElement locateElement(String locatorType, String value) {
	
			switch (locatorType) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "link": return driver.findElementByLinkText(value);
			case "tagName": return driver.findElementByTagName(value);
			case "xpath": return driver.findElementByXPath(value);
			case "css": return driver.findElementByCssSelector(value);
		
			}
			return null;
	}
	public void clearAndType(WebElement ele, String data) {
	
			ele.clear();
			ele.sendKeys(data);
			
		
	}
	
	public void click(WebElement ele) {
		ele.click();
	}
	
}


