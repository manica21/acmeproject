package testCase;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class SearchVendor {
	
	@Test
	public void findVendor() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementById("email").sendKeys("manicadevi21@gmail.com");
		driver.findElementById("password").sendKeys("manica21");
		driver.findElementById("buttonLogin").click();
		
		
		
		WebElement vendorEle = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
		
		WebElement searchVendorEle = driver.findElementByXPath("//a[text()='Search for Vendor']");
		Actions builder = new Actions(driver);
		builder.moveToElement(vendorEle).pause(2000);
		
		builder.click(searchVendorEle).perform();
		
		Thread.sleep(2000);
		
		
		driver.findElementById("vendorTaxID").sendKeys("IT231232");
		driver.findElementById("buttonSearch").click();
		
		WebElement table = driver.findElementByXPath("//table[@class='table']");
		
	 List<WebElement> row = table.findElements(By.tagName("tr"));
	 WebElement vendor = row.get(1);
	 List<WebElement> column = vendor.findElements(By.tagName("td"));
	 
	 WebElement vendorNameEle = column.get(0);
	System.out.println("the Vendor name is : " + vendorNameEle.getText());
	

		
	}
	

}
