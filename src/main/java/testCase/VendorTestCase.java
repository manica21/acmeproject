package testCase;

import org.testng.annotations.Test;

import ACMEpages.LoginPage;
import baseClass.SeleniumBase;

public class VendorTestCase extends SeleniumBase{
	
	@Test
	public void getVendorName() {
		
		new LoginPage()
		.enterMail()
		.enterPassword()
		.clickLogin()
		.clickSearchVendor()
		.searchVendor()
		.clickSearch()
		.getVendorName();
		
		
		
	}

}
