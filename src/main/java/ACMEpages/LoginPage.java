package ACMEpages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import baseClass.SeleniumBase;

public class LoginPage extends SeleniumBase {
	
	public LoginPage(){
	}

	
	public LoginPage enterMail() {
		
		WebElement eleUsername = locateElement("id", "email");
		clearAndType(eleUsername, "manicadevi21@gmail.com");
		return this;
	}
	
	public LoginPage enterPassword() {
		
		WebElement passwordEle = locateElement("id", "password");
		clearAndType(passwordEle, "manica21");
		return this;
	}
	
	public HomePage clickLogin() {
		WebElement loginEle = locateElement("id", "buttonLogin");
		click(loginEle);
		return new HomePage();
	}

}
