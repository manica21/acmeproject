package ACMEpages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseClass.SeleniumBase;

public class VendorPage extends SeleniumBase{
	
	public VendorPage() {
		
	}
	
	public void getVendorName() {
		WebElement table = driver.findElementByXPath("//table[@class='table']");
		
		 List<WebElement> row = table.findElements(By.tagName("tr"));
		 WebElement vendor = row.get(1);
		 List<WebElement> column = vendor.findElements(By.tagName("td"));
		 
		 WebElement vendorNameEle = column.get(0);
		System.out.println("the Vendor name is : " + vendorNameEle.getText());
		
	}

}
