package ACMEpages;

import org.openqa.selenium.WebElement;

import baseClass.SeleniumBase;

public class SearchVendorPage extends SeleniumBase {
	
	public SearchVendorPage() {
		
	}
	
	public SearchVendorPage searchVendor() {
		WebElement searchVendorEle = locateElement("id", "vendorTaxID");
		clearAndType(searchVendorEle, "IT231232");
		return this;
		
	}

	public VendorPage clickSearch() {
		WebElement searchEle = locateElement("id", "buttonSearch");
		click(searchEle);
		return new VendorPage();
	}
}
