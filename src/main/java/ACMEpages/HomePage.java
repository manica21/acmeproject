package ACMEpages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import baseClass.SeleniumBase;

public class HomePage extends SeleniumBase {
	
	public HomePage() {
		
	}
	
    public SearchVendorPage  clickSearchVendor() {
 		
        WebElement vendorEle = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
      
		WebElement searchVendorEle = driver.findElementByXPath("//a[text()='Search for Vendor']");
		Actions builder = new Actions(driver);
		builder.moveToElement(vendorEle).pause(2000);
		
		builder.click(searchVendorEle).perform();
		return new SearchVendorPage();

}
}
